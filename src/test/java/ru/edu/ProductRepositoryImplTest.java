package ru.edu;

import org.junit.Assert;
import org.junit.Test;
import ru.edu.Product;
import ru.edu.ProductCategory;
import ru.edu.ProductRepositoryImpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public class ProductRepositoryImplTest {

    private final ProductRepositoryImpl repository;

    public ProductRepositoryImplTest() throws SQLException {

        String jdbcUrl = "jdbc:sqlite:db/test_sqlite_database.db";
        Connection connection = DriverManager.getConnection(jdbcUrl);

        this.repository = new ProductRepositoryImpl(connection);
    }

    @Test
    public void createProductTest() {

        Product product = new Product();
        product.setId(UUID.randomUUID());
        product.setCategory(ProductCategory.COSMETICS);
        product.setName("My Product1");
        product.setDescription("Product description");
        product.setManufacturer("Manufacture");
        product.setManufactureDateTime(LocalDateTime.of(2022, 2, 20, 12, 20));
        product.setHasExpiryTime(false);

        Product actualProduct = repository.save(product);
        Assert.assertNotNull(actualProduct);

        validateEquality(actualProduct, product);

        actualProduct = repository.findById(product.getId());
        Assert.assertNotNull(actualProduct);

        validateEquality(actualProduct, product);

        repository.deleteById(product.getId());
        actualProduct = repository.findById(product.getId());
        Assert.assertNull(actualProduct);
    }

    @Test
    public void getProductsTest() {

        Product product1 = new Product();
        product1.setId(UUID.randomUUID());
        product1.setCategory(ProductCategory.COSMETICS);
        product1.setName("My Product1");
        product1.setDescription("Product description");
        product1.setManufacturer("Manufacture");
        product1.setManufactureDateTime(LocalDateTime.of(2022, 2, 20, 12, 20));
        product1.setHasExpiryTime(false);

        Product product2 = new Product();
        product2.setId(UUID.randomUUID());
        product2.setCategory(ProductCategory.TECHNIC);
        product2.setName("My Product2");
        product2.setDescription("Product2 description");
        product2.setManufacturer("Manufacture2");
        product2.setManufactureDateTime(LocalDateTime.of(2022, 2, 15, 12, 20));
        product2.setHasExpiryTime(false);

        Product actualProduct = repository.save(product1);
        Assert.assertNotNull(actualProduct);

        actualProduct = repository.save(product2);
        Assert.assertNotNull(actualProduct);

        List<Product> actualList = repository.findAllByCategory(ProductCategory.COSMETICS);
        Assert.assertEquals(1, actualList.size());
        validateEquality(product1, actualList.get(0));

        actualList = repository.findAllByCategory(ProductCategory.TECHNIC);
        Assert.assertEquals(1, actualList.size());
        validateEquality(product2, actualList.get(0));

        actualList = repository.findAllByCategory(ProductCategory.CHEMICAL);
        Assert.assertEquals(0, actualList.size());

        repository.deleteById(product1.getId());
        repository.deleteById(product2.getId());
    }

    private void validateEquality(Product actualProduct, Product product) {

        Assert.assertEquals(actualProduct.getId(), product.getId());
        Assert.assertEquals(actualProduct.getCategory(), product.getCategory());
        Assert.assertEquals(actualProduct.getName(), product.getName());
        Assert.assertEquals(actualProduct.getDescription(), product.getDescription());
        Assert.assertEquals(actualProduct.getManufacturer(), product.getManufacturer());
        Assert.assertTrue(actualProduct.getManufactureDateTime().isEqual(product.getManufactureDateTime()));
        Assert.assertEquals(actualProduct.isHasExpiryTime(), product.isHasExpiryTime());
    }
}
