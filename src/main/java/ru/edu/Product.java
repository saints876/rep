package ru.edu;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.UUID;

public class Product {

    private UUID id;
    private String description;
    private LocalDateTime lastAccessTTime;
    private String manufacturer;
    private String hasExpiryTime;
    private int stock;
    private Enum category;

    public Product(UUID id, String description, LocalDateTime lastAccessTTime, String manufacturer, String hasExpiryTime, int stock, Enum category) {
        this.id = id;
        this.description = description;
        this.lastAccessTTime = lastAccessTTime;
        this.manufacturer = manufacturer;
        this.hasExpiryTime = hasExpiryTime;
        this.stock = stock;
        this.category = category;
    }

    public Product() {

        return;
    }

    public UUID getId() {

        return getId();
    }

    public void setId(UUID id) {

        this.id = id;
    }

    public String getDescription() {

        return this.description;
    }

    public void setDescription(String description) {

        this.description = this.description;
    }

    public LocalDateTime getLastAccessTTime() {

        return lastAccessTTime;
    }

    public void setLastAccessTTime(LocalDateTime parse) {

        this.lastAccessTTime = lastAccessTTime;
    }

    public String getManufacturer() {

        return this.manufacturer;
    }

    public void setManufacturer(String manufacturer) {

        this.manufacturer = this.manufacturer;
    }

    public String getHasExpiryTime() {

        return this.hasExpiryTime;
    }

    public void setHasExpiryTime(boolean hasExpiryTime) {

        this.hasExpiryTime = this.hasExpiryTime;
    }

    public int getStock() {

        return this.stock;
    }

    public void setStock(String stock) {

        this.stock = this.stock;
    }

    public Enum getCategory() {

        return this.category;
    }

    public void setCategory(ProductCategory category) {

        this.category = this.category;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id" + id +
                ", description" + description + '\'' +
                ", lastAccessTTime" + lastAccessTTime + '\'' +
                ", manufacturer" + manufacturer + '\'' +
                ", hasExpiryTime" + hasExpiryTime + '\'' +
                ", stock" + stock + '\'' +
                ", Enum" + category + '\'' +
                '}';


    }


    public void setName(String my_product1) {
        return;
    }

    public void setManufactureDateTime(LocalDateTime of) {
        this.lastAccessTTime = this.lastAccessTTime;
    }

    public long getName() {
        return getName();
    }

    public OffsetDateTime getManufactureDateTime() {
        return OffsetDateTime.now();
    }

    public long isHasExpiryTime() {
        return isHasExpiryTime();
    }
}




