package ru.edu;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.*;

public class ProductRepositoryImpl  implements ProductRepository {

    private final Connection connection;

    public ProductRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    private final Map<UUID, Product> data = new HashMap<UUID, Product>();


    public void createTable() {
        try (Statement statement = connection.createStatement()) {
            String script = "create table 'user' (\n" +
                    "'id' varchar (30) PRIMARY KEY, \n " +
                    "'description' varchar (30) NOT NULL UNIQUE, \n" +
                    "'lastAccessTTime' varchar (30) NOT NULL, \n" +
                    "'manufacturer' varchar (30) NOT NULL, \n" +
                    "'hasExpiryTime' varchar (30) NOT NULL, \n" +
                    "'stock' varchar (30) NOT NULL, \n" +
                    "'category' varchar (30) NOT NULL, \n" +
                    ")";
            int updatedRows = statement.executeUpdate(script);
            System.out.println(".createTable completed updatedRows" + updatedRows);
        } catch (Exception ex) {
            throw new RuntimeException("Failed to .createTable error=" + ex.toString(), ex);
        }
    }


    public List<Product> getAll() {
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery("select * from product")) {
                List<Product> products = new ArrayList<>();
                while (resultSet.next()) {

                    Product product = extractProductSet(resultSet);

                    products.add(product);


                    return products;

                }

            } catch (Exception ex) {
                throw new RuntimeException("Failed to .getAll errors=" + ex.toString(), ex);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return new ArrayList<>(data.values());
    }

    private Product extractProductSet(ResultSet resultSet) throws SQLException {
        String id = resultSet.getString("id");
        String description = resultSet.getString("description");
        String lastAccessTTime = resultSet.getString("lastAccessTTime");
        String manufacturer = resultSet.getString("manufacturer");
        String hasExpiryTime = resultSet.getString("hasExpiryTime");
        String stock = resultSet.getString("stock");
        String category = resultSet.getString("category");

        Product product = new Product();
        product.setId(UUID.fromString(id));
        product.setDescription(description);
        product.setLastAccessTTime(LocalDateTime.parse(lastAccessTTime));
        product.setManufacturer(manufacturer);
        product.setHasExpiryTime(hasExpiryTime.equals(lastAccessTTime));
        product.setStock(stock);
        product.setCategory(ProductCategory.COSMETICS);
        return product;
    }


//    public Product create (Product product) {
//        try (Statement statement = connection.createStatement()){
//            int updatedRows = statement.executeUpdate(script);
//            System.out.println(".dropTable completed updatedRows" + updatedRows );
//            return findById(product.getId().orElse(null));
//
//        }
//    }

    @Override
    public Product findById(UUID id) {
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery("select * from product where id = '" + id + "'")) {
                if (!resultSet.next()) {
                    return null ;
                }

                return extractProductSet(resultSet);
            } catch (Exception ex) {

            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return findById(id);
    }
        @Override
        public void deleteById (UUID id) {
            Optional<Product> exitingProduct = Optional.ofNullable(findById(id));
            if (!exitingProduct.isPresent()) {
                throw new IllegalStateException("Product id " + id + "not found");
            }

        }
            @Override
            public Product save (Product product){
                data.put(product.getId(), product);
                return product;
            }

            @Override
            public List<Product> findAllByCategory (ProductCategory category){

                return new ArrayList<>(data.values());
            }
        }



