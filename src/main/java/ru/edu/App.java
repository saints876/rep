package ru.edu;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.UUID;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) throws JsonProcessingException, SQLException {


        Connection connection = DriverManager.getConnection("jdbc:sqlite:db/test_sqlite_database.db");
        ProductRepository productRepository = new ProductRepositoryImpl(connection);


        startProgram(productRepository);
    }


    private static void startProgram(ProductRepository productRepository) {
        ObjectMapper mapper = new ObjectMapper()
                //  ProductRepository repository =  new ProductRepositoryImpl();

                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        System.out.println("User storage.\nPut commands");

        Scanner scanner = new Scanner(System.in);

        while (true) {

            System.out.println(">");
            String line = scanner.nextLine();

            String[] items = line.split("");
            String cmd = items[0];


            switch (cmd) {
                case "FIND": {

                    try {
                        Product product = mapper.readValue(items[1], Product.class);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }

                    break;
                }


                case "DELETE": {
                    UUID uuid = UUID.fromString(items[1]);

                    break;
                }
                case "SAVE": {
                    Product product = null;
                    try {
                        product = mapper.readValue(items[1], Product.class);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                    product.getId();
                    break;
                }
            }
        }
    }
}




